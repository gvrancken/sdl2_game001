
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

static bool isRunning = true;

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

bool HandleEvent(SDL_Event *Event)
{
    switch(Event->type)
    {
        case SDL_QUIT:
        {
            printf("SDL_QUIT\n");
            isRunning = false;
        }
            break;
        
        case SDL_KEYDOWN:
        {
            int keyPressed = Event->key.keysym.sym;
            printf("key: %i", keyPressed);
            
            if (keyPressed == SDLK_SPACE) {
                isRunning = false;
            }
    } break;
            
            case SDL_WINDOWEVENT:
        {
            switch(Event->window.event)
            {
                case SDL_WINDOWEVENT_RESIZED:
                {
                    printf("Resized to %dx%d", Event->window.data1, Event->window.data2);
                } break;
            }
        } break;


    }
    
   
    return(0);
}

int main() {
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        //TODO: SDL_Init didn't work!
        SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, "Error", "SDL_Init didn't work!", 0);
    }
    
    // Initialize SDL_Image to prevent delay using first time
    if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG){
        printf("IMG_Init error %s", SDL_GetError());
        isRunning = false;
    }
    
    SDL_Window *Window;
    Window = SDL_CreateWindow("SDL Project",
                              SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED,
                              SCREEN_WIDTH,
                              SCREEN_HEIGHT,
                              SDL_WINDOW_RESIZABLE);
    if (Window == nullptr) {
        printf("error creating window");
        isRunning = false;
    }

    SDL_Renderer *renderer = SDL_CreateRenderer(Window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == nullptr){
        SDL_DestroyWindow(Window);
        printf("SDL_CreateRenderer Error: %s", SDL_GetError());
        isRunning = false;
    }
    
    char *imagePath = (char *)"test.png";
    SDL_Texture *texture = IMG_LoadTexture(renderer, imagePath);
//    SDL_FreeSurface(bmp);
    if (texture == nullptr){
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(Window);
        printf("SDL_CreateTextureFromSurface Error: %s", SDL_GetError());
        SDL_Quit();
        return 1;
    }
    
    //Setup the destination rectangle to be at the position we want
    SDL_Rect dst;
    dst.x = 0;
    dst.y = 0;
    int w, h;
    //Query the texture to get its width and height to use
    SDL_QueryTexture(texture, NULL, NULL, &w, &h);
    dst.w = 640 / 2;
    dst.h = w/h * 480 / 2;
    
//    SDL_Rect dst2 = dst;
//    dst2.x = 300;
    // Game loop
    while(isRunning)
    {
        SDL_Event Event;
        SDL_WaitEvent(&Event);
        if (HandleEvent(&Event))
        {
            break;
        }
        
        //Draw the texture
        SDL_RenderClear(renderer);

        SDL_RenderCopy(renderer, texture, NULL, &dst);
//        SDL_RenderCopy(renderer, texture, NULL, &dst2);
        //Update screen
        SDL_RenderPresent(renderer);

    }
    
    return(0);
    
}

